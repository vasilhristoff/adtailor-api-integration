$( document ).ready(function() {
    
    new Clipboard('.btn');
    
    $(".section-link-big").on("click", function(e) {
         e.preventDefault();

		$("#table-list-api").find(".scroll-link").removeClass("active-api-property");
		       
        $(this).toggleClass("active opened").find(".glyphicon").attr("class","glyphicon glyphicon-triangle-top");
        $(this).parent().find(".table-list-medium").slideToggle("fast"); 
		
		if($(this).hasClass("opened")) {
			 $(this).find(".glyphicon").attr("class","glyphicon glyphicon-triangle-top")
		}
    });
    
    $(".section-link-medium").on("click", function(e) {
		
         e.preventDefault();
		
         $(".section-link-medium").find(".glyphicon").attr("class","glyphicon glyphicon-triangle-bottom");
         $(this).toggleClass("active").find(".glyphicon").attr("class","glyphicon glyphicon-triangle-top");
         $(this).parent().find(".table-list-small").slideToggle("fast");	
         
    });
	
	

	$(".scroll-link").on("click", function(e) {
		
         e.preventDefault();
		$("#table-list-api-1,#table-list-api-2").find(".scroll-link").removeClass("active-api-property");
		$("#table-list-api").find(".scroll-link").removeClass("active-api-property");
		if( $(this).parent().parent().attr("id") == "table-list-api-1" 
		  || $(this).parent().parent().attr("id") == "table-list-api-2") {
		   $(this).addClass("active-api-property");
		}

		
		var target = $(this).attr("href");
	    var $target = $(target);

		 $(".table-api tr td").removeClass("active-td");
		$target.find("td").addClass("active-td");

	    $('html, body').stop().animate({
	        'scrollTop': $target.offset().top
	    }, 900, 'swing', function () {
	        window.location.hash = target;
	    });
	});
	
	
	$(".table-list-big > li > a .glyphicon").on("click", function() {
		$(this).parent().parent().find(".table-list-medium").slideUp();
	});
	
	
	// expand code box
	
	$(".expand-codebox-light").on("click", function() {
		$(this).parent().next().addClass("expand-class").append("<span class='close-codebox'></span>");
		$(document).find("body").css("overflow","hidden");
		$(".close-codebox").on("click", function() {
			$(this).parent().removeClass("expand-class");
			$(document).find("body").css("overflow","auto");
			$(this).remove();
		});
	});
	
	$(".expand-codebox-dark").on("click", function() { 
		$(this).parent().next().addClass("expand-class expand-class-light").append("<span class='close-codebox close-codebox-dark'></span>");
		$(document).find("body").css("overflow","hidden");
		$(".close-codebox-dark").on("click", function() {
			$(this).parent().removeClass("expand-class expand-class-light");
			$(document).find("body").css("overflow","auto");
			$(this).remove();
		});
	});
	

		$(".toggle-icon-mobile").on("click" , function() {
			$(".table-of-contents-nav").slideToggle("fast");
		});

		if( $(window).width() < 769 ) {
			$(".scroll-link").on("click" , function() {
				var section =  window.location.hash;
				if(section) {
					$("" + section + "").animate({
						'scrollTop': $("" + section + "").offset().top - 150
					});
				}	
				
				$(".table-of-contents-nav").hide();
				
			});
		}
	
});	



